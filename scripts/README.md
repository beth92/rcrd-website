## Scripts

This directory contains scripts which are used in various places on the RCRD website. The source code is contained within the `src` directory.

## Browser Compatibility

For browser compatibility, the transpiler Babel is used to transform the JS from a modern format to one which is more widely compatible with older browsers. For example it transforms syntax `const` and `let` to `var`. It also minifies the code to make it more compact on the page and obfuscated. <br>


## Running Babel
To compile with Babel the easiest way is as follows. This guide assumes you are using a bash terminal and you will require npm (> 5.6) installed on your machine. After cloning this repo to your local machine, run `npm install` to download all the required packages. You only need to do this once.<br>
You can make changes to the source code directly in the `src` folder. Once you are ready to transpile it, run the following command (from the root directory):

```bash
  npx babel <path_to_script> --out-dir ./lib/<script_name>.js --no-comments --minified
```

Replacing `path_to_script` and `script_name` with the appropriate values for the script you are changing.

## I'm stuck, help
If you are not abe to transpile with Babel as shown above, you can use Babel's [online tool](https://babeljs.io/repl) to approximate a transpilation.


## Making Config Changes
If you need to change the settings for Babel modify the `babel.config.js` file located at the project root. Documentation for the package is available [on the Babel website](https://babeljs.io/docs/en/).
