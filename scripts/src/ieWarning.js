(function() {
  // script to add a warning to the navbar if user is on IE
  const warningMessage = "Warning: we have detected that you may be using Internet Explorer. Many features on this webite may not work. For a better experience please switch to a more modern browser such as Chrome or Firefox.";

  const isIE = /Trident\/|MSIE/.test(window.navigator.userAgent);

  if (!isIE) {
    return;
  } else {
    try {
      const ieWarningDiv = jQuery("<div id='rcrd__ieWarning'></div>");
      ieWarningDiv.html("<center><p>" + warningMessage + "</p></center>");
      ieWarningDiv.css({
        "background-color": "#ffe39a",
        "color": "#000000"
      });
      const navBarElement = jQuery("#main-header");
      ieWarningDiv.prependTo(navBarElement);
    } catch (e) {
      return;
    }
  }
})();
