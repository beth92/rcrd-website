(function() {

  const checkCompatibility = () => {
    try {
      const isIE = /Trident\/|MSIE/.test(window.navigator.userAgent);
      return !!(EventTarget && Array.from && (!isIE));
    } catch (e) {
      return false;
    }
  };

  // check for crappy browsers before proceeding
  const compatible = checkCompatibility();
  if (!compatible) {
    const filtersContainer = jQuery('#rcrd__filters');
    filtersContainer.hide();
    return;
  }

  const getEvents = () => {
    const eventRows = Array.from(document.getElementsByClassName('event-row'));
    return eventRows.map((eventRow) => {
      return {
        element: eventRow,
        day: eventRow.getElementsByTagName('h1')[0].innerText,
        month: eventRow.getElementsByTagName('h3')[0].innerText.split(' ')[0],
        year: eventRow.getElementsByTagName('h3')[0].innerText.split(' ')[1],
        description: eventRow.innerText
      };
    });
  };

  const getPastEvents = (events) => {
    return events.filter(function(event) {
      var now = new Date();
      var eventDate = Date.parse(event.day + ' ' + event.month + ' ' + event.year);
      return Date.parse(now.toDateString()) > eventDate;
    });
  };

  const getNonSodaEvents = (events) => {
    const triggerWords = [' soda', 's.o.d.a', 'southern ontario derby alliance'];
    return events.filter(event => {
      return !(triggerWords.some(word => event.description.toLowerCase().indexOf(word) >= 0));
    });
  };

  const hideEvents = (eventsToHide) => {
    eventsToHide.forEach(event => {
      event.element.style.display = 'none';
    });
  };

  const showEvents = (eventsToShow) => {
    eventsToShow.forEach(event => {
      event.element.style.display = '';
    });
  };

  // event handlers
  function handleToggleChange() {
    const eventsToHide = FILTERS.reduce((acc, filter) => {
      if (filter.element.checked) {
        // returns array of past events, or of non-soda events
        return acc.concat(filter.filterFn(ALL_EVENTS));
      }
      return acc;
    }, []);
    const eventsToShow = ALL_EVENTS.filter(event => {
      return eventsToHide.indexOf(event) < 0;
    });
    hideEvents(eventsToHide);
    showEvents(eventsToShow);
  }


  // entry point
  // to add new filter, add it to this list and implement a filter fn
  // filterFn takes an array of events and returns a filtered array of events

  const FILTERS = [{
    filterFn: getPastEvents ,
    element: document.getElementById('rcrd__hide-past')
  }, {
    filterFn: getNonSodaEvents,
    element: document.getElementById('rcrd__hide-non-soda')
  }];

  const ALL_EVENTS = getEvents();

  FILTERS.forEach(filter => {
    filter.element.addEventListener('change', handleToggleChange);
  });
})();
